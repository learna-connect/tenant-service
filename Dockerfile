FROM openjdk:14-alpine
COPY build/libs/tenant-service-*-all.jar tenant-service.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "tenant-service.jar"]