package com.learna.tenant.repository;

import com.learna.tenant.model.Tenant;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.UUID;

@Repository
public interface TenantRepository extends PageableRepository<Tenant, UUID> {
}
