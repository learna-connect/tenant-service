package com.learna.tenant.repository;

import com.learna.tenant.model.TenantUser;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TenantUserRepository extends PageableRepository<TenantUser, UUID>{

    List<TenantUser> findByTenantId(UUID tenantId);

    Optional<TenantUser> findByUserIdAndTenantId(UUID userId, UUID tenantId);

}
