package com.learna.tenant.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "tenant_user")
public class TenantUser {



    @Id
    @Column(length = 16)
    private  UUID userId;

    @Column(length = 16)
    private  UUID tenantId;

    @Column(name = "first_name", length = 32)
    private String firstName;

    @Column(name = "last_name", length = 32)
    private String lastName;

    @Column(length = 32)
    private String username;

    @Column( length = 48)
    private String email;
}
