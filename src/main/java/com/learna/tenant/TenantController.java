package com.learna.tenant;

import com.learna.tenant.dto.CreateTenantRequestDTO;
import com.learna.tenant.dto.CreateUserRequestDTO;
import com.learna.tenant.dto.TenantResponseDTO;
import com.learna.tenant.dto.UserResponseDTO;
import com.learna.tenant.idp.IdentityProviderClient;
import com.learna.tenant.model.Tenant;
import com.learna.tenant.model.TenantUser;
import com.learna.tenant.repository.TenantRepository;
import com.learna.tenant.repository.TenantUserRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.swagger.v3.oas.annotations.Operation;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller("/tenants")
public class TenantController {

    @Inject
    private IdentityProviderClient client;

    protected final TenantRepository tenantRepository;

    protected final TenantUserRepository tenantUserRepository;

    private TenantMapper mapper = TenantMapper.getInstance();


    public TenantController(TenantRepository tenantRepository, TenantUserRepository tenantUserRepository) {
        this.tenantRepository = tenantRepository;
        this.tenantUserRepository = tenantUserRepository;
    }

    @Post
    @Operation(description = "Creates a new Tenant")
    public HttpResponse<TenantResponseDTO> postTenant(@Body CreateTenantRequestDTO dto){
        Tenant tenant = mapper.map(dto);
        TenantResponseDTO response = mapper.map(tenantRepository.save(tenant));
        return HttpResponse.created(response);
    }

    @Get
    @Operation(description = "Returns all the tenants created")
    public Page<TenantResponseDTO> getTenants(Pageable pageable){
        return tenantRepository.findAll(pageable)
                .map(mapper::map);
    }

    @Get("/{tenantId}")
    @Operation(description = "Get a tenant identified by Id")
    public TenantResponseDTO getTenanats(UUID tenantId){
        return mapper.map(tenantRepository.findById(tenantId).orElse(null));
    }

    @Post("/{tenantId}/users")
    @Operation(description = "Creates a user and associates a user to a particular tenant")
    public HttpResponse<UserResponseDTO> createUser(UUID tenantId, @Body CreateUserRequestDTO createUserRequestDTO){

        TenantUser tenantUser = client.createTenantUser(createUserRequestDTO,tenantId);

        if(tenantUser != null){
            tenantUserRepository.save(tenantUser);
            UserResponseDTO dto = mapper.map(tenantUser);
            return HttpResponse.created(dto);
        }

        return HttpResponse.serverError();
    }


    @Get("/{tenantId}/users")
    @Operation(description = "get all users in a particular tenant")
    public List<UserResponseDTO> getTenantUsers(UUID tenantId){
        List<TenantUser> users = tenantUserRepository.findByTenantId(tenantId);

        return users.stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Get("/{tenantId}/users/{userId}")
    @Operation(description = "Get a user defined by userId")
    public UserResponseDTO getTenantUser(UUID tenantId, UUID userId){
        Optional<TenantUser> tenantUser = tenantUserRepository.findByUserIdAndTenantId(userId,tenantId);

        return tenantUser.map(user -> mapper.map(user)).orElse(null);

    }

    @Delete("/{tenantId}/users/{userId}")
    @Operation(description = "Delete a Tenant User")
    public void deleteTenantUser(UUID tenantId, UUID userId){
        client.deleteUser(userId);
        tenantUserRepository.deleteById(userId);
    }

}
