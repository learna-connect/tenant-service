package com.learna.tenant.idp;

import com.learna.tenant.dto.CreateUserRequestDTO;
import com.learna.tenant.dto.UserResponseDTO;
import com.learna.tenant.model.TenantUser;

import java.util.List;
import java.util.UUID;

public interface IdentityProviderClient {

    /**
     * Creates a user in the identityProvider
     * @param createUserRequestDTO
     * @return
     */
    TenantUser createTenantUser(CreateUserRequestDTO createUserRequestDTO, UUID tenantId);


    /**
     * Retrieves a user given a userId
     * @param userId
     * @return
     */
    UserResponseDTO getUser(UUID userId);

    /**
     * returns a list of users in a particular tenant
     * @param tenantId
     * @return
     */
    List<UserResponseDTO> getUsers(UUID tenantId);

    /**
     * Updates a user in the identity provider
     * @param userId
     * @return
     */
    UserResponseDTO updateUser(UUID userId);

    /**
     * Deletes a user from both the identity provider and tenant_user table
     * @param userId
     * @return
     */
    boolean deleteUser(UUID userId);
}
