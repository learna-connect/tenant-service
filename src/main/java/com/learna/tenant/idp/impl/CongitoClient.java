package com.learna.tenant.idp.impl;


import com.learna.tenant.TenantMapper;
import com.learna.tenant.dto.CreateUserRequestDTO;
import com.learna.tenant.dto.UserResponseDTO;
import com.learna.tenant.idp.IdentityProviderClient;
import com.learna.tenant.model.TenantUser;
import com.learna.tenant.repository.TenantUserRepository;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.UUID;

@Singleton
public class CongitoClient implements IdentityProviderClient {

    private final String CLIENT_ID= "1jl73tl9c4bbaobca770t6lbjj";

    private final String USER_POOL_ID = "us-east-2_UoUR9ev8F";

    private CognitoIdentityProviderClient cognitoClient = CognitoIdentityProviderClient.builder()
            .region(Region.US_EAST_2)
            .build();

    private TenantMapper mapper = TenantMapper.getInstance();

    @Inject
    private TenantUserRepository tenantUserRepository;

    @Override
    public TenantUser createTenantUser(CreateUserRequestDTO createUserRequestDTO, UUID tenantId) {

        AdminCreateUserResponse response = cognitoClient.adminCreateUser(
                AdminCreateUserRequest.builder()
                        .userPoolId(USER_POOL_ID)
                        .username(createUserRequestDTO.getUsername())
                        .userAttributes(AttributeType.builder()
                                .name("username").value(createUserRequestDTO.getUsername())
                                .name("email").value(createUserRequestDTO.getEmail())
                                .name("name").value(createUserRequestDTO.getLastName())
                                .name("family_name").value(createUserRequestDTO.getFirstName())
                                .name("birthdate").value(createUserRequestDTO.getBirthdate())
                                .name("custom:tenantId").value(tenantId.toString())
                                .build())
                        .build()
        );


        List<AttributeType> attributes = response.user().attributes();

        String userId = "";

        for(AttributeType type : attributes){
            if("sub".equals(type.name())){
                userId =type.value();
                break;
            }
        }

        TenantUser tenantUser = mapper.map(createUserRequestDTO);
        tenantUser.setUserId(UUID.fromString(userId));
        tenantUser.setTenantId(tenantId);
            return tenantUser;



    }

    @Override
    public UserResponseDTO getUser(UUID userId) {
        return null;
    }

    @Override
    public List<UserResponseDTO> getUsers(UUID tenantId) {
        return null;
    }

    @Override
    public UserResponseDTO updateUser(UUID userId) {
        return null;
    }

    @Override
    public boolean deleteUser(UUID userId) {
        TenantUser user = tenantUserRepository.findById(userId).orElseThrow();

        AdminDeleteUserResponse response = cognitoClient.adminDeleteUser(
                AdminDeleteUserRequest.builder()
                        .userPoolId(USER_POOL_ID)
                        .username(user.getUsername())
                        .build()
        );
        return true;
    }
}
