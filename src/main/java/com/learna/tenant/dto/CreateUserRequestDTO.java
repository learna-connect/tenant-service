package com.learna.tenant.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateUserRequestDTO {

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String birthdate;
}
