package com.learna.tenant.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class TenantResponseDTO {


    private UUID id;


    private String name;


    private String description;
}
