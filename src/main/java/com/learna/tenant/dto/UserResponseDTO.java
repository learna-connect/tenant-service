package com.learna.tenant.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserResponseDTO {


    private UUID userId;

    private String firstName;

    private String lastName;

    private String username;

    private String email;

}
