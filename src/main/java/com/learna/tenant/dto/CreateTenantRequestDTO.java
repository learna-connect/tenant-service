package com.learna.tenant.dto;

import lombok.Data;

@Data
public class CreateTenantRequestDTO {

    private String name;

    private String description;
}
