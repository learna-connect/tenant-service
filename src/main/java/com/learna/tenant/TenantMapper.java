package com.learna.tenant;

import com.learna.tenant.dto.CreateTenantRequestDTO;
import com.learna.tenant.dto.CreateUserRequestDTO;
import com.learna.tenant.dto.TenantResponseDTO;
import com.learna.tenant.dto.UserResponseDTO;
import com.learna.tenant.model.Tenant;
import com.learna.tenant.model.TenantUser;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public abstract class TenantMapper {

    private static final TenantMapper INSTANCE = Mappers.getMapper(TenantMapper.class);

    public static TenantMapper getInstance(){
        return INSTANCE;
    }

    public abstract TenantUser map(CreateUserRequestDTO dto);

    public abstract UserResponseDTO map(TenantUser user);

    public abstract Tenant map(CreateTenantRequestDTO dto);

    public abstract TenantResponseDTO map(Tenant tenant);
}
